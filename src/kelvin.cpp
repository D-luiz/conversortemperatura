#include <iostream>
#include "kelvin.hpp"
#include <math.h>

using namespace std;

Kelvin :: Kelvin(){

    setTemperatura(0);

}
Kelvin :: Kelvin(float temperatura){

    setTemperatura(temperatura);

}


float Kelvin::converteC(){ 
    
    return (getTemperatura() - 273);
}

float Kelvin::converteF(){ 
    
    return 9*(getTemperatura() - 273)/5 -32;
}


