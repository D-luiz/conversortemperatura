#include <string>
#include <iostream>
#include "kelvin.hpp"
#include "celsius.hpp"
#include "fahrenheit.hpp"

using namespace std;

int main(){
     
    float temperature,result;  
    int choose, contador = 0;
  
    cout << "Escolha o tipo de conversão: "<< endl;
    cout << "1 - Celsius para Kelvin e Fahrenheit. " << endl;
    cout << "2 - Fahrenheit para Celsius e Kelvin. " << endl;
    cout << "3 - Kelvin para Celsius e Fahrenheit. " << endl; 
   // cin >> choose;
    
    do{ 
    cin >> choose;
    if(choose == 1){
    cout << "Digite o valor da temperatura em Celsius: "<< endl;
    cin >> temperature;
    contador++;
   
    }else if(choose == 2){
    cout << "Digite o valor da temperatura em Fahrenheit: "<< endl;
    cin >> temperature;
    contador++;

    }else if(choose == 3){
    cout << "Digite o valor da temperatura em Kelvin: "<< endl;
    cin >> temperature;
    contador++;

    }else{
    cout << "Opção inválida" << endl;    
    }
    }while(contador == 0);
   
    if(choose == 1){ 
 
    Celsius * temperatura1 = new Celsius(temperature);
    result = temperatura1->converteK();
    cout << "Temperatura em  Kelvin: " << result << endl;
    result = temperatura1->converteF();
    cout << "Temperatura em Fahrenheit: " << result << endl;

    }else if(choose == 2){
    Fahrenheit * temperatura2 = new Fahrenheit(temperature);
    result = temperatura2->converteC(); 
    cout << "Temperatura em celsius: " << result << endl;
    result = temperatura2->converteK();
    cout << "Temperatura em Kelvin: " << result << endl;

    }else if(choose == 3){
    Kelvin * temperatura3 = new Kelvin(temperature);
    result = temperatura3->converteC();
    cout << "Temperatura em Celsius: " << result << endl;
    result = temperatura3->converteF();
    cout << "Temperatura em Fahrenheit: " << result << endl;
    }

return 0;
}
