#include <iostream>
#include "fahrenheit.hpp"
#include <math.h>

using namespace std;

Fahrenheit :: Fahrenheit(){

    setTemperatura(0);

}
Fahrenheit :: Fahrenheit(float temperatura){

    setTemperatura(temperatura);

}
float Fahrenheit::converteK(){ 
    
    return (5*(getTemperatura() -32)/9)+273;
}

float Fahrenheit::converteC(){ 
   
    return 5*(getTemperatura()-32)/9;
}


