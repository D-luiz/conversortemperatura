#ifndef CELSIUS_HPP
#define CELSIUS_HPP
#include <string>
#include "medidor_temperatura.hpp"

using namespace std;

class Celsius:public medidortemperatura{
public:
      Celsius();
      Celsius(float temperatura);
      float converteK();
      float converteF();

};
#endif 
